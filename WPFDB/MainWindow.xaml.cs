﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace WPFDB
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        DataSet Products;
        DataSet Providers;
        DataSet Customers;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["storage"].ConnectionString);

        void UpdateComboBox(ComboBox box, SqlCommand com)
        {
            con.Open();
            List<string> list = new List<string>();
            var reader = com.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(reader.GetString(0));
                }
            }
            box.ItemsSource = list;
            con.Close();
        }
        ////////////////////////////////////////////////
        #region Заказчики

        private void AddCustButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("insert into Customers values (@name, @addr, @pho)", con);
            com.Parameters.AddWithValue("@name", NewCustName.Text);
            com.Parameters.AddWithValue("@addr", NewCustAddr.Text);
            com.Parameters.AddWithValue("@pho", NewCustPhone.Text);
            com.ExecuteScalar();
            con.Close();
            CustomersTab_Selected(this, null);
        }

        private void DelCustName_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Name from Customers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void DelCustPhone_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Phone from Customers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void DelCustAddr_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Address from Customers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void EditOldCustName_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Name from Customers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void EditOldCustPhone_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Phone from Customers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void EditOldCustAddr_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Address from Customers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void DelCustButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("delete from Customers where CustomerId = @id", con);
            com.Parameters.AddWithValue("@id",CustId.Content);
            com.ExecuteScalar();
            con.Close();
            CustomersTab_Selected(this, null);
        }

        private void FilterCustButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("Select CustomerId as id,Name as Название, Address as Адрес, Phone as Телефон from Customers where Name like @name and Phone like @pho and Address like @addr", con);
            com.Parameters.AddWithValue("@name", "%" + FilterCustName.Text + "%");
            com.Parameters.AddWithValue("@pho", "%+" + FilterCustPhone.Text + "%");
            com.Parameters.AddWithValue("@addr", "%" + FilterCustAddr.Text + "%");
            var dataAdapter = new SqlDataAdapter(com);
            Customers = new DataSet();
            dataAdapter.Fill(Customers);
            CustGrid.ItemsSource = Customers.Tables[0].DefaultView;
            con.Close();
        }

        private void EditCustButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            if(EditNewCustName.Text != "")
            {
                SqlCommand com = new SqlCommand("Update Customers set Name = @name  where CustomerId = " + CustId.Content, con);
                com.Parameters.AddWithValue("@name", EditNewCustName.Text);
                com.ExecuteScalar();
            }
            if(EditNewCustAddr.Text != "")
            {
                SqlCommand com = new SqlCommand("Update Customers set Address = @addr  where CustomerId = " + CustId.Content, con);
                com.Parameters.AddWithValue("@addr", EditNewCustAddr.Text);
                com.ExecuteScalar();
            }
            if(EditNewCustPhone.Text != "")
            {
                SqlCommand com = new SqlCommand("Update Customers set Phone = @pho  where CustomerId = " + CustId.Content, con);
                com.Parameters.AddWithValue("@pho", EditNewCustPhone.Text);
                com.ExecuteScalar();
            }
            con.Close();
            CustomersTab_Selected(this, null);
        }


        private void CustomersTab_Selected(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("Select CustomerId as id,Name as Название,Address as Адрес, Phone as Телефон  from Customers", con);
            var dataAdapter = new SqlDataAdapter(com.CommandText, con);
            Customers = new DataSet();
            dataAdapter.Fill(Customers);
            CustGrid.ItemsSource = Customers.Tables[0].DefaultView;
            con.Close();
        }

        #endregion
        ////////////////////////////////////////////////
        #region Товары
        private void AddProduct_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("AddProduct @prodType, @manufacturer, @name, @conditions, @pack, @time", con);
            com.Parameters.AddWithValue("@prodType", AddProdPrType.Text);
            com.Parameters.AddWithValue("@manufacturer", AddProdMan.Text);
            com.Parameters.AddWithValue("@name", AddProdName.Text);
            com.Parameters.AddWithValue("@conditions", AddProdCond.Text);
            com.Parameters.AddWithValue("@pack", AddProdPack.Text);
            com.Parameters.AddWithValue("@time", AddProdTime.Text);
            com.ExecuteScalar();
            con.Close();
            ProductsTab_Selected(this, null);
        }

        private void CurrProdName_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Name from Products", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void CurrProdType_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Name from ProductTypes", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void CurrProdMan_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Manufacturer from Products", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void FilterProductsButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("Select ProductId as id, Products.Name as 'Имя товара', Manufacturer as Производитель, Package as Упаковка, ShelfTime as 'Срок годности', StorageConditions as 'Условия хранения', ProductTypes.Name as 'Тип товара' from Products inner join ProductTypes on Products.ProductTypeId = ProductTypes.ProductTypeId where Products.Name like @name and Manufacturer like @man", con);
            com.Parameters.AddWithValue("@name", "%" + FilterProdName.Text + "%");
            com.Parameters.AddWithValue("@man", "%" + FilterProdMan.Text + "%");
            var dataAdapter = new SqlDataAdapter(com);
            Products = new DataSet();
            dataAdapter.Fill(Products);
            ProdGrid.ItemsSource = Products.Tables[0].DefaultView;
            con.Close();
        }

        private void EditProductButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            if (null != NewProdPrType.SelectedItem.ToString())
            {
                SqlCommand com = new SqlCommand("Update Products set ProductTypeId = (select top(1) ProductTypeId from ProductTypes where Name = @name) where ProductId = @id",con);
                com.Parameters.AddWithValue("@name",NewProdPrType.SelectedItem.ToString());
                com.Parameters.AddWithValue("@id", ProductId.Content);
                com.ExecuteScalar();
            }
            if(NewProdName.Text != "")
            {
                SqlCommand com = new SqlCommand("Update Products set Name = @name where ProductId = @id", con);
                com.Parameters.AddWithValue("@name", NewProdName.Text);
                com.Parameters.AddWithValue("@id", ProductId.Content);

                com.ExecuteScalar();
            }
            if(NewProdCond.Text != "")
            {
                SqlCommand com = new SqlCommand("Update Products set StorageConditions = @cond where ProductId = @id", con);
                com.Parameters.AddWithValue("@cond", NewProdCond.Text);
                com.Parameters.AddWithValue("@id", ProductId.Content);
                com.ExecuteScalar();
            }
            if(NewProdPack.Text != "")
            {
                SqlCommand com = new SqlCommand("Update Products set Package = @pack where ProductId = @id", con);
                com.Parameters.AddWithValue("@pack", NewProdPack.Text);
                com.Parameters.AddWithValue("@id", ProductId.Content);
                com.ExecuteScalar();
            }
            if(NewProdMan.Text != "")
            {
                SqlCommand com = new SqlCommand("Update Products set Manufacturer = @man where ProductId = @id", con);
                com.Parameters.AddWithValue("@man", NewProdMan.Text);
                com.Parameters.AddWithValue("@id", ProductId.Content);
                com.ExecuteScalar();
            }
            if(NewProdTime.Text != "")
            {
                SqlCommand com = new SqlCommand("Update Products set ShelfTime = @time where ProductId = @id", con);
                com.Parameters.AddWithValue("@time", NewProdTime.Text);
                com.Parameters.AddWithValue("@id", ProductId.Content);
                com.ExecuteScalar();
            }
            con.Close();
            ProductsTab_Selected(this, null);
        }

        private void DelProductButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("delete from Products where ProductId = @id", con);
            com.Parameters.AddWithValue("@id", ProductId.Content);
            com.ExecuteScalar();
            CustGrid.Items.Refresh();
            con.Close();
            ProductsTab_Selected(this, null);
        }

        private void DelProdNames_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Name from Products", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void ProductsTab_Selected(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("ShowProducts", con);
            var dataAdapter = new SqlDataAdapter(com.CommandText, con);
            Products = new DataSet();
            dataAdapter.Fill(Products);
            ProdGrid.ItemsSource = Products.Tables[0].DefaultView;
            con.Close();
        }
        #endregion
        ////////////////////////////////////////////////
        #region Поставщики

        private void AddProvButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("AddProvider @name, @addr, @tel", con);
            com.Parameters.AddWithValue("@name", AddProvName.Text);
            com.Parameters.AddWithValue("@addr", AddProvAddr.Text);
            com.Parameters.AddWithValue("@tel", AddProvPhone.Text);
            com.ExecuteScalar();
            con.Close();
            ProvidersTab_Selected(this, null);
        }

        private void CurrProvName_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Name from Providers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void CurrProvAddr_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Address from Providers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void CurrProvTel_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Phone from Providers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void EditProvButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            if(NewProvName.Text != "")
            {
                SqlCommand com = new SqlCommand("update Providers set Name = @name where ProviderId = @id", con);
                com.Parameters.AddWithValue("@name",NewProvName.Text);
                com.Parameters.AddWithValue("@id",ProviderId.Content);
                com.ExecuteScalar();
            }
            if(NewProvAddr.Text != "")
            {
                SqlCommand com = new SqlCommand("update Providers set Address = @addr where ProviderId = @id", con);
                com.Parameters.AddWithValue("@addr", NewProvAddr.Text);
                com.Parameters.AddWithValue("@id", ProviderId.Content);
                com.ExecuteScalar();
            }
            if(NewProvTel.Text != "")
            {
                SqlCommand com = new SqlCommand("update Providers set Phone = @pho where ProviderId = @id", con);
                com.Parameters.AddWithValue("@pho", NewProvTel.Text);
                com.Parameters.AddWithValue("@id", ProviderId.Content);
                com.ExecuteScalar();
            }
            con.Close();
            ProvidersTab_Selected(this, null);
        }

        private void DeleteProvButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("delete from Providers where ProviderId = @id", con);
            com.Parameters.AddWithValue("@id", ProviderId.Content);
            con.Close();
            ProvidersTab_Selected(this, null);
        }

        private void DelProvName_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Name from Providers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void DelProvAddr_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Address from Providers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void DelProvTel_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Phone from Providers", con);
            UpdateComboBox((ComboBox)sender, com);
        }

        private void FilterProvButton_Click(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("Select ProviderId as id Name as Название, Address as Адрес, Phone as Телефон from Providers where Name like @name and Phone like @pho and Address like @addr", con);
            com.Parameters.AddWithValue("@name", "%" + FilterProvName.Text + "%");
            com.Parameters.AddWithValue("@pho", "%+" + FilterProvTel.Text + "%");
            com.Parameters.AddWithValue("@addr", "%" + FilterProvAddr.Text + "%");
            var dataAdapter = new SqlDataAdapter(com);
            Providers = new DataSet();
            dataAdapter.Fill(Providers);
            ProvGrid.ItemsSource = Providers.Tables[0].DefaultView;
            con.Close();
        }

        private void ProvidersTab_Selected(object sender, RoutedEventArgs e)
        {
            con.Open();
            SqlCommand com = new SqlCommand("Select ProviderId as id,  Name as Название, Address as Адрес, Phone as Телефон from Providers", con);
            var dataAdapter = new SqlDataAdapter(com.CommandText, con);
            Providers = new DataSet();
            dataAdapter.Fill(Providers);
            ProvGrid.ItemsSource = Providers.Tables[0].DefaultView;
            con.Close();
        }

        #endregion

        private void NewProdPrType_DropDownOpened(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("select distinct Name from ProductTypes", con);
            UpdateComboBox((ComboBox)(sender), com);
        }

        private void CustGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                CustId.Content = Customers.Tables[0].Rows[CustGrid.SelectedIndex][0].ToString();
            }
            catch
            {

            }
            
        }

        private void ProdGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                ProductId.Content = Products.Tables[0].Rows[ProdGrid.SelectedIndex][0].ToString();
            }
            catch
            {

            }
        }

        private void ProvGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                ProviderId.Content = Providers.Tables[0].Rows[ProvGrid.SelectedIndex][0].ToString();
            }
            catch
            {

            }
        }
    }
}
